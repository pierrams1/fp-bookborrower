@extends('layouts.master_admin')

@section('content')
<div class="container">
    <h1>{{$book->title}} ({{$book->year}})</h1>
    <img src="{{asset('picture/'.$book->picture)}}" alt="" style="height: 300px; width:300px;">
    <h5 class="mt-3">Writer: {{$book->writer}}</h5>
    <h5>Publisher: {{$book->publisher}}</h5>
    <h5>Year of Publish: {{$book->year}}</h5>
    <h5>Status: {{$book->status}}</h5>
    <h5>Genre:</h5>
    @forelse ($genres as $item => $genre)
        <button class="btn btn-primary me-2">{{$genre->name}}</button>
    @empty
        
    @endforelse
    <br><br>
    <h5>Synopsis:</h5>
    <p>{{$book->synopsis}}</p>
    <br><br>
    <a href="/book/{{$book->id}}/edit"><button class="btn btn-primary">edit</button></a>
</div>
@endsection