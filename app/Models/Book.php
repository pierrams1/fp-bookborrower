<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table = 'books';

    protected $fillable = ['title','writer','publisher','year','synopsis','genre','status','picture'];

    public $timestamps = false;

    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genre');
    }

    public function borrow()
    {
        return $this->hasMany(Borrow::class);
    }
}
