<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Genre;
use App\Models\Profile;
use App\Models\Borrow;
use Illuminate\Support\Facades\Auth;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    { 
        $this->middleware('auth')->except('indexNonAuth','show');
    }

    public function indexNonAuth()
    {
        $books = Book::where('status', 'Available')->get();
        return view('layouts.user_interface.index', compact('books'));
    }

    public function indexAdmin()
    {
        $auth = Auth::user();
        if($auth->role === 'user'){
            return redirect('/borrow');
        }
        $borrows = Borrow::get();
        return view('layouts.admin.table_borrow', compact('borrows'));
    }

    public function index()
    {
        $books = Book::where('status', 'Available')->get();
        $auth = Auth::user();
        if($auth->role === 'Admin'){
            return view('layouts.admin.index', compact('books'));
        }
        return view('layouts.user_interface.index', compact('books'));
    }

    public function store($book_id){
        $book = Book::find($book_id);
        if($book->status === 'Unavailable'){
            return redirect('/borrow/'.$book_id)->with(['warning' => 'the Book is Unavailable']);
        }
        $auth = Auth::user();
        $borrow = Borrow::create([
            'book_id' => $book->id,
            'user_id' => $auth->id,
            'date_borrow' => date('Y-m-d'),
            'date_return' => date('Y-m-d'),
        ]);
        if(!$borrow){
            dd($borrow);
        }
        $book_update = Book::find($book_id)->update([
            'status' => 'Unavailable'
        ]);
        return redirect('/borrow'.$book_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $book = Book::where('id', $id)->first();
        $genres = Genre::select('name')->where('book_id', $id)->get();
        return view('layouts.user_interface.detail_book', compact('book', 'genres'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // dd($id);
        $borrow = Borrow::where('book_id', $id)->first();
        // dd($borrow->user->nama, $borrow->book->title);
        $borrow_update = Book::where('id', $id)->update([
            'status' => 'has return',
        ]);
        $delete = $this->destroy($id);
        if(!$delete){
            dd($delete);
        }
        return redirect('/borrow');
    }

    public function destroy($id)
    {
        $query = Borrow::where('book_id',$id)->delete();
        return $query;
    }
}
