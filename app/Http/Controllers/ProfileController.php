<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use File;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        // dd($request->all(), auth::user()->id);
        $auth_id = auth::user()->id;
        $request->validate([
            'phone_number' => 'required',
            'address' => 'required',
            'picture' => 'required|image|mimes:png,jpg,jpeg',
        ]);
        $fileName = time().'.'.$request->picture->extension();
        $request->picture->move(public_path('picture/profile'), $fileName);
        Profile::create([
            'phone_number' => $request["phone_number"],
            'address' => $request["address"],
            'picture' => $fileName,
            'user_id' => $auth_id
        ]);
        return redirect('/profile/'.$auth_id)->with('success', 'Book berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $auth = Auth::user();
        $profile = Profile::where('user_id', $id)->first();
        // dd($profile);
        if($profile === null){
            return view('layouts.user_interface.create_profile');
        }
        return view('layouts.user_interface.show_profile', compact('profile','auth'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $profile = Profile::where('user_id', $id)->first();
        return view('layouts.user_interface.edit_profile', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $auth_id = auth::user()->id;
        $request->validate([
            'phone_number' => 'required',
            'address' => 'required',
            'picture' => 'image|mimes:png,jpg,jpeg',
        ]);
        $profile = Profile::where('user_id',$auth_id)->first();
        // dd($profile->picture);
        if ($request->has('picture')) {
            $path = 'picture/profile/';
            File::delete($path. $profile->picture);
            $fileName = time().'.'.$request->picture->extension();
            $request->picture->move(public_path('picture/profile'), $fileName);
        }
        Profile::where('user_id',$auth_id)->update([
            'phone_number' => $request["phone_number"],
            'address' => $request["address"],
            'picture' => $fileName,
        ]);
        return redirect('/profile/'.$auth_id)->with('success', 'Berhasil update data!');
    }
}
