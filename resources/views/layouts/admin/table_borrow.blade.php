@extends('layouts.master_admin')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">Books Table</h4>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <a href="/book/create">
                    <button class="btn btn-primary mb-3">Create New Book</button>
                </a>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Name Borrower</th>
                            <th>Status</th>
                            <th>data borrowed</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($borrows as $item => $borrow)
                        <tr>
                            <td>{{$item + 1}}</td>
                            <td>{{$borrow->book->title}}</td>
                            <td>{{$borrow->user->nama}}</td>
                            @if ($borrow->book->status === 'Unavailable')
                            <td>borrowed</td>
                            @elseif($borrow->book->status === 'Available')
                            <td>Available</td>
                            @else
                            <td>has return</td>
                            @endif
                            <td>{{$borrow->date_borrow}}</td>
                            <td class="d-flex">
                                <a href="/borrow/{{$borrow->book->id}}/edit" class="btn btn-primary btn-sm mr-2">return</a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5" align="center">No Borrow</td>
                        </tr>    
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection