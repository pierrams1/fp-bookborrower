<nav class="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
    <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between">
        <a class="navbar-brand brand-logo-mini align-self-center d-lg-none" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a>
        <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize">
            <i class="mdi mdi-menu"></i>
        </button>
        <ul class="navbar-nav">
            <li class="nav-item nav-search border-0  d-none d-md-flex">
            <form class="nav-link form-inline mt-2 mt-md-0">
                <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" />
                <div class="input-group-append">
                    <span class="input-group-text">
                    <i class="mdi mdi-magnify"></i>
                    </span>
                </div>
                </div>
            </form>
            </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right ml-lg-auto">
            @guest
            <li class="nav-item nav-profile dropdown border-0">
                <div class="col-5">
                    <a href="{{route('login')}}"><button class="btn btn-primary">Login</button></a>
                </div>
                <div class="col-8">
                    <a href="{{route('register')}}"><button class="btn btn-warning">register</button></a>
                </div>
            </li>
            @endguest
            @auth
            <li class="nav-item nav-profile dropdown border-0">
                <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown">
                    <img class="nav-profile-img mr-2" alt="" src="{{asset('assets/images/faces/face1.jpg')}}" />
                    <span class="profile-name">{{Auth::user()->nama}}</span>
                </a>
                <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
            @endauth
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>