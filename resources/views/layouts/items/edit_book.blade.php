@extends('layouts.master_admin')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">Edit {{$book->title}}</h4>
    <div class="card">
        <div class="card-body">
            <p class="card-description"></p>
            <form class="forms-sample" method="POST" action="/book/{{$book->id}}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ old('title', $book->title) }}" placeholder="Title">
                </div>
                @error('title')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="writer">Writer</label>
                    <input type="text" name="writer" class="form-control" id="writer" value="{{ old('writer', $book->writer) }}" placeholder="writer">
                </div>
                @error('writer')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="publisher">Publisher</label>
                    <input type="text" name="publisher" class="form-control" id="publisher" value="{{ old('publisher', $book->publisher) }}" placeholder="Publisher">
                </div>
                @error('publisher')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="year">Year</label>
                    <input type="number" name="year" class="form-control" id="year" value="{{ old('year', $book->year) }}" placeholder="year">
                </div>
                @error('year')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <label for="">genre</label><br>
                <div class="form-group">
                    <input class="form-radio-input" type="checkbox" value="Romance" id="flexCheckDefault" name="genre[]"/>
                    <label class="form-radio-label" for="flexCheckDefault">Romance</label>
                    <input class="form-radio-input" type="checkbox" value="Action" id="flexCheckDefault" name="genre[]"/>
                    <label class="form-radio-label" for="flexCheckDefault">Action</label>
                    <input class="form-radio-input" type="checkbox" value="Horor" id="flexCheckDefault" name="genre[]"/>
                    <label class="form-radio-label" for="flexCheckDefault">Horor</label>
                    <input class="form-radio-input" type="checkbox" value="Humor" id="flexCheckDefault" name="genre[]"/>
                    <label class="form-radio-label" for="flexCheckDefault">Humor</label>
                </div>
                @error('genre')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="picture">Picture</label>
                    <input type="file" name="picture" class="form-control" value="{{ old('picture', $book->picture) }}" id="picture" placeholder="picture">
                </div>
                @error('picture')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="synopsis">synopsis</label>
                    <input type="text" name="synopsis" value="{{ old('synopsis', $book->synopsis) }}" id="synopsis" cols="30" rows="10" class="form-control" placeholder="synopsis">
                </div>
                @error('synopsis')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2"> Submit </button>
            </form>
        </div>
    </div>
</div>
@endsection