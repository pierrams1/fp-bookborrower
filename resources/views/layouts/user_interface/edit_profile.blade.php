@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">Edit {{$profile->user->nama}}</h4>
    <div class="card">
        <div class="card-body">
            <p class="card-description"></p>
            <form class="forms-sample" method="POST" action="/profile/{{$profile->user->id}}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="phone_number">Phone Number</label>
                    <input type="text" name="phone_number" class="form-control" id="phone_number" value="{{ old('phone_number', $profile->phone_number) }}" placeholder="phone_number">
                </div>
                @error('phone_number')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="address">address</label>
                    <input type="text" name="address" value="{{ old('address', $profile->address) }}" id="address" cols="30" rows="10" class="form-control" placeholder="address">
                </div>
                @error('address')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="picture">Picture</label>
                    <input type="file" name="picture" class="form-control" value="{{ old('picture', $profile->picture) }}" id="picture" placeholder="picture">
                </div>
                @error('picture')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2"> Submit </button>
            </form>
        </div>
    </div>
</div>
@endsection