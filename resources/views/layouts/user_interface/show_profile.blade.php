@extends('layouts.master')

@section('content')
<div class="container">
    <h1>{{$auth->nama}}</h1>
    <div class="container d-flex mb-5" style="padding: 0">
        <div class="col-4" style="padding: 0">
            <img src="{{asset('picture/profile/'.$profile->picture)}}" alt="" class="img-profile">
        </div>
        <div class="col-6">
            <br>
            <h5>role: {{$auth->role}}</h5><br>
            <h5>email: {{$auth->email}}</h5><br>
            <h5>phone number: {{$profile->phone_number}}</h5><br>
            <h5>address: {{$profile->address}}</h5><br>
        </div>
    </div>
    <a href="/profile/{{$auth->id}}/edit"><button class="btn btn-primary">edit</button></a>
</div>
@endsection