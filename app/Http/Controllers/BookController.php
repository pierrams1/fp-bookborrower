<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    { 
        $this->middleware('auth')->except('show');
    }

    public function index()
    {
        //
        $books = Book::get();
        // dd($books->all());
        return view('layouts.items.table_book', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('layouts.items.form_book');
    }

    public function store_genre($request, $arr){
        $request = $this->search($request['title']);
        // dd($request);
        for ($i=0; $i < count($arr); $i++) { 
            $genre = Genre::create([
                'name' => $arr[$i],
                'book_id' => $request
            ]);
        };
        if($genre){
            return true;
        }
        return false;
    }

    public function search($title){
        $query = Book::where('title', $title)->first();
        return $query->id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'required|unique:books',
            'writer' => 'required',
            'publisher' => 'required',
            'year' => 'required',
            'synopsis' => 'required',
            'genre' => 'required',
            'picture' => 'required|image|mimes:png,jpg,jpeg',
        ]);
        $arr = $request['genre'];
        $fileName = time().'.'.$request->picture->extension();
        $request->picture->move(public_path('picture'), $fileName);
        // dd($arr);
        Book::create([
            'title' => $request["title"],
            'writer' => $request["writer"],
            'publisher' => $request["publisher"],
            'year' => $request["year"],
            'synopsis' => $request["synopsis"],
            'status' => 'Available',
            'picture' => $fileName,
        ]);
        $result = $this->store_genre($request, $arr);
        if (!$result) {
            dd($result);
        }
        return redirect('/book')->with('success', 'Book berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $book = Book::where('id', $id)->first();
        $genres = Genre::select('name')->where('book_id', $id)->get();
        $auth = Auth::user();
        if($auth->role === 'Admin'){
            return view('layouts.items.detail_book', compact('book', 'genres'));
        }
        // dd($book->all(), $genres);
        return view('layouts.user_interface.detail_book', compact('book', 'genres'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // dd($id);
        $book = Book::where('id', $id)->first();
        return view('layouts.items.edit_book', compact('book'));
    }

    public function delete_genre($id){
        $request = Genre::where('book_id', $id)->delete();
        return $request;
    }

    public function update_genre($request, $arr){
        $request = $this->search($request['title']);
        $success = $this->delete_genre($request);
        if(!$success){
            return false;
        }
        // dd($success);
        for ($i=0; $i < count($arr); $i++) { 
            $genre = Genre::create([
                'name' => $arr[$i],
                'book_id' => $request
            ]);
        };
        if($genre){
            return true;
        }
        return false;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all(), $id);
        $request->validate([
            'title' => 'required',
            'writer' => 'required',
            'publisher' => 'required',
            'year' => 'required',
            'synopsis' => 'required',
            'genre' => 'required',
            'picture' => 'image|mimes:png,jpg,jpeg',
        ]);
        $arr = $request['genre'];
        $book = Book::find($id);
        // dd($book->picture);
        if ($request->has('picture')) {
            $path = 'picture/';
            File::delete($path. $book->picture);
            $fileName = time().'.'.$request->picture->extension();
            $request->picture->move(public_path('picture'), $fileName);
        }
        $query = Book::where('id', $id)->update([
            'title' => $request["title"],
            'writer' => $request["writer"],
            'publisher' => $request["publisher"],
            'year' => $request["year"],
            'synopsis' => $request["synopsis"],
            'status' => 'Available',
            'picture' => $fileName,
        ]);
        $success = $this->update_genre($request, $arr);
        if (!$success) {
            dd($success);
        }
        return redirect('/book')->with('success', 'Berhasil update data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $path = 'picture/';
        $book = Book::find($id);
        File::delete($path. $book->picture);
        $query = Genre::where('book_id',$id)->delete();
        $query = Book::where('id',$id)->delete();
        return redirect('/book')->with('success', 'Berhasil delete data!');
    }

    public function updateStatus($id){
        // dd($id);
        $book = Book::find($id)->update([
            'status' => 'Available',
        ]);
        return redirect('/book');
    }
}
