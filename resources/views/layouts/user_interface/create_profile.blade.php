@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">Create New Profile</h4>
    <div class="card">
        <div class="card-body">
            <p class="card-description"></p>
            <form class="forms-sample" method="POST" action="/profile" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="phone_number">Phone Number</label>
                    <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="phone number">
                </div>
                @error('phone_number')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="address">address</label>
                    <textarea name="address" id="address" cols="30" rows="5" class="form-control" id="address" placeholder="address"></textarea>
                </div>
                @error('address')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <div class="form-group">
                    <label for="picture">Picture</label>
                    <input type="file" name="picture" class="form-control" id="picture" placeholder="picture">
                </div>
                @error('picture')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2"> Submit </button>
            </form>
        </div>
    </div>
</div>
@endsection