@extends('layouts.master_admin')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">Books Table</h4>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <a href="/book/create">
                    <button class="btn btn-primary mb-3">Create New Book</button>
                </a>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Writer</th>
                            <th>Publisher</th>
                            <th>Year</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($books as $item => $book)
                        <tr>
                            <td>{{$item + 1}}</td>
                            <td>{{$book->title}}</td>
                            <td>{{$book->writer}}</td>
                            <td>{{$book->publisher}}</td>
                            <td>{{$book->year}}</td>
                            <td class="d-flex">
                                <a href="/book/{{$book->id}}" class="btn btn-info btn-sm mr-2">detail</a>
                                <a href="/book/{{$book->id}}/edit" class="btn btn-default btn-sm mr-2">edit</a>
                                <a href="/book/{{$book->id}}/has_available" class="btn btn-primary btn-sm mr-2">has return</a>
                                <form action="/book/{{$book->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">No Book</td>
                        </tr>    
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection