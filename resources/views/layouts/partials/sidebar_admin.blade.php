<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        @auth
        <li class="nav-item nav-profile">
            <a href="/profile/{{Auth::user()->id}}" class="nav-link"> 
                {{-- bisa dibuat untuk profile ^^^ --}}
            <div class="nav-profile-image">
                <img src="{{asset('assets/images/faces/face1.jpg')}}" alt="profile" />
                <span class="login-status online"></span>
                <!--change to offline or busy as needed-->
            </div>
            <div class="nav-profile-text d-flex flex-column pr-3">
                <span class="font-weight-medium mb-0">{{Str::limit(Auth::user()->nama, 15)}}</span>
            </div>
            {{-- <span class="badge badge-danger text-white ml-3 rounded">3</span> --}}
            </a>
        </li>
        @endauth
        <li class="nav-item">
            <a class="nav-link" href="/borrow">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        @guest
        <li class="nav-item">
            <a class="nav-link" href="/book">
                <i class="mdi mdi-table-large menu-icon"></i>
                <span class="menu-title">Avalaibe Book</span>
            </a>
        </li>
        @endguest
        @auth
        <li class="nav-item">
            <a class="nav-link" href="/book">
                <i class="mdi mdi-table-large menu-icon"></i>
                <span class="menu-title">Books Table</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/book/create">
                <i class="mdi mdi-book-open-outline menu-icon"></i>
                <span class="menu-title">Create New Book</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/borrow_table">
                <i class="mdi mdi-book-open-variant menu-icon"></i>
                <span class="menu-title">Borrows Table</span>
            </a>
        </li>
        @endauth
            {{-- <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li> --}}
        </ul>
</nav>