<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\BorrowController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BorrowController::class, 'indexNonAuth']);

Route::get('/borrow/{borrow_id}/borrowed', [BorrowController::class, 'store']);

Route::get('/borrow_table', [BorrowController::class, 'indexAdmin']);

Route::get('/book/{book_id}/has_available', [BookController::class, 'updateStatus']);

Route::resource('book', BookController::class);

Route::resource('profile', ProfileController::class)->middleware('auth');

Route::resource('borrow', BorrowController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
