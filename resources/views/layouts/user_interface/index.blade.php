@extends('layouts.master')

@section('content')
<div class="container-fluid d-flex">
    @forelse ($books as $item => $book)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('picture/'.$book->picture)}}" class="card-img-top" alt="..." style="height: 300px;">
            <div class="card-body">
              <h5 class="card-title">{{$book->title}}</h5>
              <p class="card-text">{{Str::limit($book->synopsis,20)}}</p>
              <a href="/borrow/{{$book->id}}" class="btn btn-primary">detail</a>
            </div>
        </div>
    </div>
    @empty
    <div class="container">
        <h1>there is no book Avalaibe</h1>    
    </div>   
    @endforelse
</div>  
@endsection